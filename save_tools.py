#!/usr/bin/python3
import json

def save_var_as_json(variable, var_name_str):
    with open('saved_%s.json'%var_name_str, 'w') as saved_file:
    	json.dump(variable, saved_file)

def open_var_from_json(var_name_str):
    with open('saved_%s.json'%var_name_str) as saved_file:
    	saved_variable = json.load(saved_file)
    return saved_variable

def save_as_json(variable, file_json):
    with open(file_json, 'w') as saved_file:
    	json.dump(variable, saved_file)

def open_from_json(file_json):
	with open(file_json) as saved_file:
		saved_variable = json.load(saved_file)
	return saved_variable

def write_list(list_name, output_file_name):
    """ Write a list vraible in a file.
    
    :param list_name: The name of the list variable to write in the file.
    :param output_file_name: The name of the file to create."""
    with open(output_file_name, 'w') as file:
        for i in list_name:
            file.write(i + "\n")
    return
