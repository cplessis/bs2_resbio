#%%
#!/usr/bin/python3
import interactome
import save_tools
import time

t0 = time.time()

x = interactome.Interactome('data_interactome/Human_HighQuality.txt')

z = x.weighted_cooccurrence_graph('n', 2)
print("Number of related proteins to 'N' graph : ",  len(z.n_prot_graph_list))
interactome.save.write_list(z.n_prot_graph_list, 'n_prot_graph_list2.txt')

z = x.weighted_cooccurrence_graph('n', 4)
print("Number of related proteins to 'N' graph : ",  len(z.n_prot_graph_list))
interactome.save.write_list(z.n_prot_graph_list, 'n_prot_graph_list4.txt')

z = x.weighted_cooccurrence_graph('n', 8)
print("Number of related proteins to 'N' graph : ",  len(z.n_prot_graph_list))
interactome.save.write_list(z.n_prot_graph_list, 'n_prot_graph_list8.txt')

z = x.weighted_cooccurrence_graph('n', 16)
print("Number of related proteins to 'N' graph : ",  len(z.n_prot_graph_list))
interactome.save.write_list(z.n_prot_graph_list, 'n_prot_graph_list16.txt')

z = x.weighted_cooccurrence_graph('n', 32)
print("Number of related proteins to 'N' graph : ",  len(z.n_prot_graph_list))
interactome.save.write_list(z.n_prot_graph_list, 'n_prot_graph_list32.txt')

z = x.weighted_cooccurrence_graph('n', 64)
print("Number of related proteins to 'N' graph : ",  len(z.n_prot_graph_list))
interactome.save.write_list(z.n_prot_graph_list, 'n_prot_graph_list64.txt')

z = x.weighted_cooccurrence_graph('n', 128)
print("Number of related proteins to 'N' graph : ",  len(z.n_prot_graph_list))
interactome.save.write_list(z.n_prot_graph_list, 'n_prot_graph_list128.txt')

#print("##################   1   ###################")
#a = interactome.DomainGraph()
#a.generate_cooccurrence_graphe_np(1)
#print("\n length of graph: ", len(a.np_domains_graph_list))
#print("number of vertices: ", len(a.get_graph_nr_vertex(a.np_domains_graph_list)))
#print(a.density(a.np_domains_graph_list))
#
#print("##################   2   ###################")
#b = interactome.DomainGraph()
#b.generate_cooccurrence_graphe_np(2)
#print("\n length of graph: ", len(b.np_domains_graph_list))
#print("number of vertices: ", len(b.get_graph_nr_vertex(b.np_domains_graph_list)))
#print(b.density(b.np_domains_graph_list))
#
#print("##################   4   ###################")
#c = interactome.DomainGraph()
#c.generate_cooccurrence_graphe_np(4)
#print("\n length of graph: ", len(c.np_domains_graph_list))
#print("number of vertices: ", len(c.get_graph_nr_vertex(c.np_domains_graph_list)))
#print(c.density(c.np_domains_graph_list))
#
#print("##################   8   ###################")
#d = interactome.DomainGraph()
#d.generate_cooccurrence_graphe_np(8)
#print("\n length of graph: ", len(d.np_domains_graph_list))
#print("number of vertices: ", len(d.get_graph_nr_vertex(d.np_domains_graph_list)))
#print(d.density(d.np_domains_graph_list))
#x = interactome.Interactome("data_interactome/Human_HighQuality.txt")
#x.xlink_uniprot('data_interactome/Human_Proteome.txt')
#x.init_dict = save_tools.open_from_json('saved_new_init_dict.json')
#x.xlink_domains()
#save_tools.save_var_as_json(x.init_dict, "init_dict")

#x.init_dict = save_tools.open_var_from_json("init_dict")
#domain_list = x.ls_domains()
#print("DOMAIN list :", len(domain_list))
#print("DOMAINS EXAMPLES", domain_list[4], domain_list[12])
#n_domains_list = x.ls_domains_n(3)
#print("N DOMAIN", len(n_domains_list)) 
#
#print(x.co_occurence("PF17838", "PF00621"))
#    
#print("DITRIBUTION", x.nb_domains_by_prot_distri())
#
#z = interactome.DomainGraph()
#
#print(x.domains_dict)
#print("DOMAIN DICT", x.get_protein_domain('INSR_HUMAN'))

t1 = time.time()
print("\n \n executed in :",t1 - t0)

# %%
