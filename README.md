# Proteins Interactions with Python

## Objective
The aim of this project is to develop tools in order to analyze the interactions between proteins.

## Week1 : Reading a "protein-protein" interactions graph
Developping some basics function in order to read and stock the information in an interaction file. 

## Week2 : Exploring the interactions "protein-protein" in the graph
Creation of functions in order to count, identify and find others informations about the proteins.

## Week3 : From Pyton to Oriented Object Python
Transforming the all the code already done into a unique class Interactome. The objective is to make the code more efficient while calculating. 

## Week4 : Calculation of the connected component between protein

## Week5 :

## Week6 :
### Question topologique 1
Used following command :
```
x = DomainGraph()
x.generate_cooccurence_graphe()
x.desnity()
```
Answer : 0.0014843928631344118

### Question topologique 2
Used following command :
```
x.neighbors_max(10)
```
Answer : ['zf-C2H2', 'fn3', 'I-set', 'EGF_CA', 'Pkinase', 'KRAB', 'EGF', 'WD40', 'Ank_2', 'hEGF']

### Question topologique 3
Used following command :
```
x.neighbors_min(10)
```
Answer : ['NOA36', 'D123', 'YIF1', 'Yip1', 'Thioredox_DsbH', 'Glypican', 'Vps53_N', 'Vps52', 'Vps35', 'FAM117']
More than 10 domains have no neighbors. 

### Question topoligique 4
Used following command :
```
x.most_frequent_dom(10)
```
Answer : ['zf-C2H2', 'fn3', 'WD40', 'I-set', 'Pkinase', 'RRM_1', 'EGF_CA', 'Ank_2', 'PDZ', 'Collagen']
It seems not to be the same list of proteins than the 10 proteins which have the highest number of neighbors. 

### Question topologique 5
Used following command :
```
x.coocurrence_self()
```
Answer : 399


