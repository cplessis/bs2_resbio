#!/usr/bin/python3
import re
import tempfile
import urllib.request
import sys
import ssl
import numpy
import json
import time
import matplotlib.pyplot as plt
import bashplot.bashplot as bp
import save_tools as save
from progress.bar import ShadyBar

###########################################################################################################################################
#################### CLASS Interactome ####################################################################################################
###########################################################################################################################################

class Interactome:
    def __init__(self, file):
        """:param file: The file which will be analyzed. """
        self.file_name = str(file) 		# We stock a global variable with the file name
        self.__is_interaction_file(file)		# We first check if the file is readable to continue the analyses.
        self.init_dict = self.__read_interaction_file_dict(file) 	# Then we create the dictionnary of vertices with all their interactions
        self.init_list = self.__read_interaction_file_list(file)	# Here we create the list of all the interactions.
        self.proteins_list = self.__get_all_proteins(self.init_dict)	# Finally we create the list of all the proteins (vertices).
        
    def __is_interaction_file(self, file_name) :
        """ This function checks if the inputed file is an interaction file. 
        Verify if the first line is a number of interactions,
        if this number fit with the real number of interactions
        and if the file as two column. The answer is not return but print in the terminal. 

        :param file_name: The name of the interaction file. """
        file = open(file_name)                          
        nb_of_loop = 0
        nb_of_interaction = 0
        column_missing = False
        for line in file.readlines() :          
            if nb_of_interaction == 0:          # The loop starts by checking the first line.
                try:                                    
                    nb_of_interaction = int(line)   # The number of interactions become the number at this first line.
                except ValueError :                          # If an error of type "ValueError" occurs, then the function stop the loop.
                    print("The file",file_name,"has NO NUMBER of interactions")  # And says that there is no number of interaction at the first line.
                    break
                
            else :
                if len(line.split()) == 2 :   # If the file has 2 comlumns, the number of loops is incremented. 
                    nb_of_loop += 1             
                else :              # Else, an error is detected on the columns and the following message is displayed. 
                    print("The file as one or more column missing or superfluous", "\n", "The file is NOT readable ")
                    column_missing = True
        if (nb_of_loop == nb_of_interaction) & (nb_of_loop != 0) :  # If the number of lines is equal to the number of interactions and is not null,
            print("The file", file_name, "has ", nb_of_interaction, " interactions" ) # a validation message is displayed.
        else :
            if column_missing == False :    # If NOT and the column missing is not the cause, the following message is displayed.
                print("One or more lines are missing or the number of interactions is false", "\n", "The file is NOT readable ")
        return

    def __read_interaction_file_dict(self, file_name):
        """This function is reading a file of molecules interactions and return a dictionnary.
        This dictionnary contains all the summits of an interaction graph as key and all the related molecules to this key. 
        Then each key is related to multiple molecules. 

        :param file_name: The name of the interaction file.
        :return dictionnary: The dictionnary of all the summits."""
        
        file = open(file_name)
        dictionnary = {}
        for line in file.readlines()[1:]:   # For each line in the file excepted the first one (number of interactions),
            vertice_1 = line.split()[0]     # we associate the first protein of the line to a vertice.
            vertice_2 = line.split()[1]     # Same thing for the second protein.
            if not(vertice_1 in dictionnary):   # If the vertice_1 is not in the dictionnary,
                dictionnary[vertice_1] = []     # it is added in.
            if not(vertice_2 in dictionnary[vertice_1]):    # If the vertice_2 is not in the list of protein which interact with the vertice_1,
                dictionnary[vertice_1].append(vertice_2)        # it is added in list associated with the key : 'vertice_1'.
            if not(vertice_2 in dictionnary):       # If 'vertice_2' is not in the dictionnary as a key, 
                dictionnary[vertice_2] = []         # it is added in.
            if not(vertice_1 in dictionnary[vertice_2]):    # If 'vertice_1' is not in the list associated to the key 'vertice_2',
                dictionnary[vertice_2].append(vertice_1)    # it is added in. 
        file.close()
        return dictionnary
    
    def __read_interaction_file_list(self, file) :
        """This function is reading a file of molecules intereactions and return a list of interactions.

        :param file_name: The name of the interaction file.
        :return interactions_list: The list of all the interactions. """
        file = open(file)
        interactions_list = []
        for line in file.readlines()[1:] :          # The loop starts at the second line of the file (the first line is the number of interactions)
            interactions_list.append(line.split())      # Then it add each molecule in a list of pairs in the whole list.
        file.close()
        return interactions_list

    def __get_all_proteins(self, dictionnary):
        """This function creates a list of proteins in the file.

        :param dictionnary: A dictionnary of all the vertices of an interactions file.  
        :return proteins_list: The list of all the proteins in the inputed dictionnary."""
        proteins_list = []
        for key in dictionnary :        # For each vertice (key) in the dictionnary.
            proteins_list.append(key)       # The vertice is added to the list of proteins. 
        return proteins_list

    def count_vertices(self):
    	"""This function counts the number of vertices in the graphe.

		:param file: the interaction file.
		:return nb_of_vertices: An integer, the number of vertices. """
    	nb_of_vertices = len(self.init_dict)  # By counting the length of the dictionnary we know how many vertices are in the graph
    	return nb_of_vertices

    def count_edges(self):
    	"""Calculates the number of edge in the graph of interactions.

		:param file: the interaction file.
		:return nb_of_edges: An integer, the number of edges."""
    	nb_of_edges = len(self.init_list) # By counting the length of the list of interactions we know how many edges there are in the file.
    	return nb_of_edges

    def clean_interactome(self):
    	"""This function read the input file to clean it by deleting the homo-dimers, the reverse interactions and the doublings. 

		:param file: the interaction file.
		:param new_file: the name of the file wich will be created. """
    	try:
    		new_interactome_file = open("clean_inter_file.txt", "x")  # Creation of the new interactome file
    	except FileExistsError:
    		print("!!!!!  FileExistsError  !!!!! ---> A file named 'clean_inter_file.txt' already exists.\nPlease remove or rename the existing file before processing this function.")
    		exit()
    	
    	new_interaction_list =[]
    	for interaction in self.init_list :        # This loop is checking if the molecule doesn't interact with it self.
    	    if interaction[0]!= interaction[1] :               # If so, the interaction is added to the new list.
    	        new_interaction_list.append(interaction)
    	n = 0
    	for interaction_to_check in new_interaction_list :      # This loop check if an interaction doesn't occur twice.
    	    reverse_interaction = [interaction_to_check[1], interaction_to_check[0]] # It also verify the reverse interaction  
    	    i = n+1
    	    for current_interaction in new_interaction_list[i:] :   # We verify in the new list of interaction,
    	        if current_interaction == interaction_to_check :        # if there are multiple same interactions,
    	            new_interaction_list.remove(current_interaction)    # if there are, they are removed from the list.
    	        if current_interaction == reverse_interaction :         # Here is the same thing but for the reverse interactions. 
    	            new_interaction_list.remove(current_interaction)
    	        i += 1          # The incrementation here is necessary to make the shift on the reading frame. 
    	    n += 1          # Ence, the loop in the loop allows to fix one molecule and compare it with all the others after it.
    	new_interactome_file.write(str(len(new_interaction_list)) + "\n")   # Here, is added the number of interactions at the top of the file
    	for interaction in new_interaction_list :
    	    new_interactome_file.write(str(interaction[0] + " " + interaction[1] + "\n")) # Here the writing of all the interactions, line by line.
    	return

    def get_degree(self, prot_str):
        """This function gives the degree of an inputed protein.

        :param prot_str: The protein for which we research its degree.
        :return degree_int: The degree of the inputed protein. """
        try:
            degree_int = len(self.init_dict[prot_str])
        except KeyError:
            print("!!!!!  KeyError  !!!!! ---> The inputed protein in get_degree() is not in the file.\nPlease indicate a valid protein.")
            exit()
        return degree_int

    def get_max_degree(self):
    	""" Return the degree maximum of the interactome by a tuple : (vertice, degree). 

    	:return max_degree: Tuple containing the vertice and its degree"""
    	max_degree = ("", 0) 	# Initialization of the reference vertice and degree.
    	for vertice in self.init_dict:		# We check all the vertices in the dictionnary.
    	    degree = len(self.init_dict[vertice])	# Initialization of the checked degree. 
    	    if degree > max_degree[1]:		# If the checked degree is higher than the initialzed one,
    	        max_degree = (vertice, degree)		# The max_degree takes the value of the current vertice and its degree.
    	    if (degree == max_degree[1]) & (vertice != max_degree[0]) :	# If the checked degree is equal to the higher one,
    	        max_degree = max_degree + (vertice, degree)		# the associated vertice is added to the tuple with its degree.
    	return max_degree

    def get_ave_degree(self):
    	"""This function returns the average degree of an interactome list.

    	:return ave_degree: The average degree of the interactome."""
    	sum_degrees = 0
    	for vertice in self.init_dict:		# For each vertice in the dictionnary, 
    	    sum_degrees += len(self.init_dict[vertice])		# we add the number of interactions (degree) to the counter.
    	ave_degree = sum_degrees/len(self.init_dict)	# Then we divide the total number of interaction by the total number of vertices. 
    	return ave_degree

    def count_degree(self, deg_int):
    	"""Count the number of proteins who have the same degree as the inputed one. 

    	:return (nb_prot_int, proteins_list): A tuple with the number of proteins with the inputed degree and the lists of all these proteins"""
    	nb_prot_int = 0
    	proteins_list = []
    	for vertice_str in self.init_dict:		# For each vertice in the dictionnary,
    	    if  len(self.init_dict[vertice_str]) == deg_int:	# we check if the current degree is the same as the inputed degree. 
    	        nb_prot_int += 1		# if so, the sum of the degrees is incrmented.
    	        proteins_list.append(vertice_str) # And the name of the vertice is added to the list. 
    	return (nb_prot_int, proteins_list)

    def histogram_degree(self, dmin_int, dmax_int):
    	"""Display an histogrma in the console which shows the number of proteins (each '*' ) who have the indicated degree. 
    	The function also returns the number of proteins cointained in the interval inputed.
        
    	:param dmin_int: The degree minimum of the interval.
    	:param dmax_int: The degree maximum of the interavl.
    	:return nb_prot_int: The number of proteins in this interval. """
    	nb_prot_int = 0
    	for vertice_str in self.init_dict:		# For each vertice in the dictionnary,
    	    if  dmin_int <= len(self.init_dict[vertice_str]) <= dmax_int:	# the function verify if its degree is in the interval.
    	        nb_prot_int += 1		# If so, the total number of protein is incremented. 
    	degree_int = dmin_int
    	while degree_int <= dmax_int: 	# Here we create the histogram beetween dmin (here 'degree') and dmax. 
    	    degree_int += 1		# So while we are going through the different degrees in the interval, the function is running the following loop.
    	    histo_line_str = str(degree_int) + " "		# Each line is a string composed of the degree level + 'space' + 'n*'
    	    for vertice_str in self.init_dict:		# each vertice in the dictionnary is checked. 
    	        if len(self.init_dict[vertice_str]) == degree_int: # Each time a vertice has the same degree as the current degree,
    	            histo_line_str += "*"		# the line of the current degree grains one '*'.
    	    print(histo_line_str)		# When all the vertices have been verified for this degree, the line is printed.
    	return nb_prot_int

    def count_cc(self):
        """Creates a dictionnary containing all the connected components (cc) of the graph. 
        Each key is the number of the cc, this key is related to a tuple in which we can find :
        the number of proteins in the cc, the list of proteins in the cc.

        :return cc_dict: {cc_number:(cc_proteins_number, cc_proteins_list)} """
        cc_dict = {}
        remain_prot_list = list(self.proteins_list)
        cc_key_int = 1
        while len(remain_prot_list) != 0:
            cc_dict[cc_key_int] = [remain_prot_list[0]]
            for prot1_str in cc_dict[cc_key_int]:
                for prot2_str in self.init_dict[prot1_str]:
                    if (prot2_str not in cc_dict[cc_key_int]) & (prot2_str in remain_prot_list):
                        cc_dict[cc_key_int].append(prot2_str)
                        remain_prot_list.remove(prot2_str)
                if prot1_str in remain_prot_list:
                    remain_prot_list.remove(prot1_str)
            cc_key_int = cc_key_int + 1
        for cc_key_int in cc_dict:
            cc_dict[cc_key_int] = (len(cc_dict[cc_key_int]), cc_dict[cc_key_int])
        self.cc_dict = cc_dict
        return cc_dict

    def write_cc(self):
        """Create a file with all the connected components from the interactome file.
        The name of the output file will be as 'connected_components_ + your_interactome_file_name'."""
        try:
            new_file = open("connected_components_"+self.file_name, "x")  # Creation of the new file
            self.cc_dict
        except FileExistsError:
            print("!!!!!  FileExistsError  !!!!! ---> A file named 'connected_components_'"+self.file_name+"already exists.\nPlease remove or rename the existing file before processing this function.")
            exit()
        except AttributeError:
            self.count_cc() 
        cc_dict = self.cc_dict
        for cc_key_int in cc_dict:
            new_file.write(str(cc_dict[cc_key_int][0]) + " " + str(cc_dict[cc_key_int][1]) + "\n")
        new_file.close()
        return

    def extract_cc(self, prot_str):
        """Research the connected components (CC) of a wanted proteins and returns all the proteins in this CC.

        :param prot_str: The wanted protein.
        :return cc_prot_list: The list of proteins in the same CC than prot_str. """
        try:
            self.cc_dict
            self.init_dict[prot_str]
        except AttributeError:
            self.count_cc()
        except KeyError:
            print("!!!!!  KeyError  !!!!! ---> The inputed protein in extract_cc() is not in the file.\nPlease indicate a valid protein.")
            exit()
        cc_prot_dict = self.cc_dict
        cc_prot_list=[]
        for cc_key_int in cc_prot_dict:
            if prot_str in cc_prot_dict[cc_key_int][1]:
                cc_prot_list = cc_prot_dict[cc_key_int][1]
                break
        return cc_prot_list

    def compute_cc(self):
        """Return a list in which the each number is related to the connected component (CC) of the protein
        in protein_list at the same position. More detailed : x.compute_cc()[i], return the CC of the proteins_list[i]. 

        :return lcc_list: The list of related CC."""
        lcc_list = []
        for prot_str in self.proteins_list:
            cc_list = list(self.extract_cc(prot_str))
            for cc_key_int in self.cc_dict:
                if cc_list == self.cc_dict[cc_key_int][1]:
                    lcc_list.append(cc_key_int)
        return lcc_list
    
    def density(self):
        """Calculate the density of the protein graph.

        :return D_float: The density graph's density."""
        D_float = (2*len(self.init_list))/(len(self.proteins_list)*(len(self.proteins_list)-1))
        print("Density of the graph : ", D_float)
        return D_float

    def clustering(self, prot_int):
        """Calculate the coefficent clustering of the wanted protein.

        :param prot_int: The wanted protein for the calculation.
        :return cluster_coef_float: The coefficient clustering of prot_int."""
        degree_int = self.get_degree(prot_int)
        cluster_coef_float = 0
        if degree_int > 1:
            linked_neighbors_int = 0
            for neighbor_str in self.init_dict[prot_int]:
                for neighbor2_str in self.init_dict[neighbor_str]:
                    if neighbor2_str in self.init_dict[prot_int]:
                        linked_neighbors_int = linked_neighbors_int + 1
            cluster_coef_float = linked_neighbors_int/(degree_int*(degree_int-1))
        else:
            print("The degree is equal or lower than 1")
        print("Local clustering coefficient for protein", prot_int, " : ", cluster_coef_float)
        return cluster_coef_float

    def xlink_uniprot(self, file_str):
        """"Create a new key 'uniprot_id' for init_dict[proteinX] variable.
        This new key has as value the uniprot ID of the proteinX. The inputed file must be a uniprot Proteome file.txt.
        Presented as: column1 = Entry; column2 = Entry name; column3 = Protein names.

        :param file_str: The file.txt in which the protein's related uniprot ID are written."""
        proteome = _Proteome(file_str)
        for protein in self.init_dict:
            try: 
                self.init_dict[protein] = \
                    {'neighbors': self.init_dict[protein], \
                        'uniprot_id': proteome.prot_name_upid_dict[protein]}
            except KeyError:
                try:
                    self.init_dict[protein] = \
                        {'neighbors': self.init_dict[protein], \
                            'uniprot_id': proteome.upid_prot_name_dict[protein]}
                except KeyError:
                    self.init_dict[protein] = \
                        {'neighbors': self.init_dict[protein], 'uniprot_id': 'NA'}
        return

    def get_unknown_prot(self):
        """Find the list of all the unknown proteins from the interactome.

        :return na_protein_list: The list of all unknown proteins"""
        na_protein_list = []
        for protein in self.init_dict:
            if self.init_dict[protein]['uniprot_id'] == 'NA':
                na_protein_list.append(protein)
        return na_protein_list

    def get_no_dom_prot(self):
        """Find all the proteins which doesn't have any known domain.
        
        :return no_dom_prot_list: The list of all prots without domains."""
        no_dom_prot_list = []
        for protein in self.proteins_list:
            for domain in self.init_dict[protein]['domains']:
                if (domain == 'NA') & (self.init_dict[protein]['uniprot_id'] != 'NA'):
                    no_dom_prot_list.append(protein)
        return no_dom_prot_list

    def get_protein_domain(self, uniprot_id):
        """Return a dictionnary with as key the name of the uniprot_id related domains 
        and as values the pfam domain ID and the number domains within the protein.

        :param uniprot_id: The UniProt ID of the wanted protein.
        :return domain_dict: {domain_name_str : [domain_id_str, number_of_domains_int]} """
        answer = True
        n = 0
        while answer == True:
            try:
                url = 'https://www.uniprot.org/uniprot/'+uniprot_id+'.txt'
                context = ssl.SSLContext(ssl.PROTOCOL_TLS)
                with urllib.request.urlopen(url, context = context) as response:
                    with tempfile.TemporaryFile() as temp_file:
                        temp_file.write(response.read())
                        temp_file.seek(0)
                        domains_list = re.findall(r'(?<=Pfam; )\w+; [\w\-]+; \d+', str(temp_file.read()))
                        domains_dict = {}
                        if len(domains_list) != 0:
                            for domain in domains_list:
                                domain_properties = domain.split("; ")
                                domains_dict[domain_properties[1]] = [domain_properties[0], int(domain_properties[2])]
                        else:
                            domains_dict['NA'] = ['NA', 0]    
                        answer = False
            except:
                n += 1
                if n > 50:
                    print("The connection to the server has failed when arriving at uniprot ID", \
                            uniprot_id, ", please check your internet conection.")
                    break
                continue
        return domains_dict

    def xlink_domains(self):
        """Add a new key 'domains' to init_dict. This key is related all the domains within the proteinX init_dict[proteinX].
        The informations are directly taken on the uniprot website. A internet connection is needed for this function. """
        with ShadyBar('Domain linking is processing', max=len(self.init_dict)) as bar:
            for protein in self.init_dict:
                if self.init_dict[protein]['uniprot_id'] != 'NA':
                    try:
                        self.init_dict[protein]['domains']           
                    except KeyError:
                        self.init_dict[protein]['domains']= self.get_protein_domain(self.init_dict[protein]['uniprot_id'])
                else:
                    self.init_dict[protein]['domains']= {'NA':['NA', 0]}
                bar.next()
        return 

    def ls_proteins(self):
        """ Return the list of non-redundant proteins within the 'protein-protein' interaction graph.

        :return nr_proteins_list: The non-redundant list of proteins. """
        nr_proteins_list = list()    # non-redondant protein list
        nr_proteins_list.append(self.proteins_list)  # equal to all the proteins from the graph       
        return nr_proteins_list

    def weighted_cooccurrence_graph(self, graph, n_int):
        """" Create a new graph of domains interactions. The domains graph can have differents parameters. 
        The function creates a new file of interactions with a third column 'number of interaction occurence', the wheight of interaction. 

        :param graph: The style of graph. IF 'n' -> the domains interactions with themself are counted and specify a threshold n_int.
        IF 'np' -> domains interactions with themself not counted and specify a threshold n_int. 
        IF 'global' -> domains interactions with themself not counted and threshold n_int not taked into account.
        :param n_int: The threshold of minimum interactions occurence for the new graph.
        :return domains_graph: The domains graph object."""
        t0 = time.time()
        self.weighted_dom_gra = []
        domains_graph = DomainGraph()
        if graph == 'n':
            domains_graph_list = domains_graph.generate_cooccurrence_graphe_n(n_int)
        if graph == 'np':
            domains_graph_list = domains_graph.generate_cooccurrence_graphe_np(n_int)
        if graph == 'global':
            domains_graph_list = domains_graph.generate_cooccurrence_graphe()
        with open('%s_weighted_graph.csv'%(graph+str(n_int)), 'w' ) as file:
            bar = ShadyBar('CSV file with weighted graph is being generated ... ', max=len(domains_graph_list))
            for interaction in domains_graph_list:
                if graph == 'n':
                    cooccur = domains_graph.cooccurrence_multi(interaction[0], interaction[1])
                else:
                    cooccur = domains_graph.cooccurrence(interaction[0], interaction[1])
                self.weighted_dom_gra.append([interaction, cooccur])            
                file.write(interaction[0]+";"+interaction[1]+";"+str(cooccur)+"\n")
                bar.next()
        t1 = time.time()
        print("\n   ==> The weighted graph CSV file has been generated in %f seconds."%(t1 - t0))
        return domains_graph

    def get_web_page(self, molecule_type_str, molecule_name_str):
        """ Get the URL of the wanted domain (pfam database) or protein (uniprot database).
        
        :param molecule_type_str: 'domain' or 'protein', the type of molecule.
        :param molecule_name_str: The name of the molecule.
        :return url: The url of the domain (pfam) or the protein (uniprot)."""
        self.init_dict = save.open_var_from_json('init_dict')            
        url = "NO URL because of a KeyError"
        try:
            if molecule_type_str == 'protein':
                url = 'https://www.uniprot.org/uniprot/'+self.init_dict[molecule_name_str]['uniprot_id']
            elif molecule_type_str == 'domain':
                domain_graph = DomainGraph()
                url = 'http://pfam.xfam.org/family/'+domain_graph.domains_dict[molecule_name_str]['pfam_id']
            else:
                print("Please write as first parameter 'protein' or 'domain'.")
        except KeyError:
                    print("KEY ERROR:  Please enter a valid molecule name.")
        return url

###################################################################################################################################################
##################### Private CLASS Proteome ######################################################################################################
###################################################################################################################################################

class _Proteome :
    """ Hidden Class used to manage the creation of the whole interactome by adding creating a Proteome. """
    def __init__(self, file_str):
        """:param file_str: The proteome file.txt from uniprot. """
        self.file_name = str(file_str)
        self.proteome_prot_list = self.get_proteome_prot_list(file_str)
        self.prot_name_upid_dict = self.get_prot_name_upid_dict(file_str)
        self.upid_prot_name_dict = self.get_upid_prot_name_dict(file_str)
    
    def get_proteome_prot_list(self, file_str):
        """ Create the list of protein in the proteome file.
        
        :param file_str: The proteome file from uniprot.
        :return proteins_list: The list of proteins from the proteome file."""
        file = open(file_str)
        proteins_list = []
        for line in file.readlines()[1:]:
            proteins_list.append(line.split()[1])
        file.close()
        return proteins_list

    def get_prot_name_upid_dict(self, file_str):
        """ Create the dictionnary of protein with the uniprot ID from the proteome file.
        The key is the protein and the value is it's related uniprot ID. 
        
        :param file_str: The proteome file from uniprot.
        :return uid_dict: The dictionnary of proteins and their related uniprot ID.  """
        file = open(file_str)
        uid_dict = {}   
        for line in file.readlines()[1:]:
            uid_dict[line.split()[1]] = line.split()[0]
        file.close()
        return uid_dict

    def get_upid_prot_name_dict(self, file_str):
        """ Create the dictionnary of uniprot ID with the protein from the proteome file.
        The key is the uniprot ID and the value is it's related protein. 
        
        :param file_str: The proteome file from uniprot.
        :return uid_dict: The dictionnary of uniprot ID and their related prottein. """
        file = open(file_str)
        uid_dict = {}
        for line in file.readlines()[1:]:
            uid_dict[line.split()[0]] = line.split()[1]
        file.close()
        return uid_dict

######################################################################################################################################################
##################### Private CLASS DomainGraph ######################################################################################################
######################################################################################################################################################

class DomainGraph():
    """ DomainGraph is used to represent the interactions between domains within a protein interactome. """    
    def __init__(self):
        self.default_json_file = 'saved_init_dict.json'
        self.init_dict = save.open_from_json(self.default_json_file)
        self.domains_list = self.ls_domains()
        self.domains_dict = {}
        self.__fill_domains_dict()

    def __fill_domains_dict(self):
        """ Create the 'domain_dict' in which all the informations of the domains interactome are saved. """
        self.__xlink_dom_id_and_prot()
        self.__xlink_dom_neighbors()
        return 

    def __xlink_dom_id_and_prot(self):
        """ Return the dictionnary of all the protein's domains as keys and subdictionnary with 'proteins' and 'pfam_id' as keys.
        The values for each key are the number of occurence first, and then all the proteins related to the domain. """
        domains_dict = {}
        for protein in self.init_dict:
            for domain in self.init_dict[protein]['domains']:
                if (domain not in domains_dict) & (domain != 'NA'):
                    domains_dict[domain] = {'pfam_id':self.init_dict[protein]['domains'][domain][0]}
                    domains_dict[domain]['proteins'] = [self.init_dict[protein]['domains'][domain][1], protein]
                elif domain != 'NA':
                    domains_dict[domain]['proteins'][0] += self.init_dict[protein]['domains'][domain][1]
                    domains_dict[domain]['proteins'].append(protein)
        self.domains_dict = domains_dict
        return

    def get_occ(self, protein_str, domain_str):
        """ Get the number of occurence of the inputed domain within the inputed protein.
        
        :param protein_str: The protein where the domain is.
        :param domain_str: The domain for which is return the number of occurence.
        :return nb_occurrence: The number of occurence of the domain in the protein."""
        nb_occurrence = 0
        if domain_str in self.init_dict[protein_str]["domains"]:
            nb_occurrence = self.init_dict[protein_str]["domains"][domain_str][1]
        return nb_occurrence

    def get_max_occ(self, domain_str):
        """" Get the maximum number of occurrence of a domain in a protein. And find this protein.
        
        :param domain_str: The domain for which we research the max occurrence in a protein. 
        :return max_occ_list: A list with as first value the maximum number of occurence and second value the related protein."""
        max_occ_list = []
        max_occ = 0
        for protein in self.init_dict:
            if max_occ < self.get_occ(protein, domain_str):
                max_occ = self.get_occ(protein, domain_str)
                max_occ_list = [self.get_occ(protein, domain_str), protein]
        return max_occ_list

    def ls_domains(self):
        """ Return the non-redundant list of protein's domains within the whole interactome. 

        :return domains_list: The non-redundant list of domains from the interactome"""
        try:
            self.domains_dict
        except AttributeError:
            self.__fill_domains_dict()
        domains_list =[]
        for domain in self.domains_dict:
            domains_list.append(domain)
        return domains_list
    
    def ls_domains_n(self, n_int):
        """ Return the non-redundant list of protein's domains with at least "n" occurences within the whole interactome.

        :param n_int: The minimum threshold of domains occurences. 
        :return n_domains_list: The non-redundant list of domains from the interactome with minimum occurence threshold 'n_int'."""
        try:
            self.domains_dict
        except AttributeError:
            self.__fill_domains_dict()
        n_domains_list = []
        for domain in self.domains_dict:
            if self.domains_dict[domain][0] >= n_int: # Check if the domain is in more than n protein in the interactome
                n_domains_list.append(domain)
        return n_domains_list

    def get_graph_nr_vertex(self, graph_list):
        """ Return the non-redundant list of domains or protein from a graph.You must create a graph_list variable to use this function.
        Please use one of the generate_cooccurrence_graphe-_n-_np functions.
        
        :param graph_list: The interaction graph list to anaylse.
        :return nr_ver tices_list: The non_redundant list of vertices from the inputed graph."""
        nr_vertices_list = []
        for interaction in graph_list:
            if interaction[0] not in nr_vertices_list:
                nr_vertices_list.append(interaction[0])
            if interaction[1] not in nr_vertices_list:
                nr_vertices_list.append(interaction[1])
        return nr_vertices_list

    def __nb_proteins_by_dom_distri(self):
        """
        Return a dictionnary in which the keys are the number of proteins 
        !!!!!!!!!!!!!! TO DO !!!!!!!!!!!!!!!!
        """
        pass

    def nb_domains_by_prot_distri(self):
        """
        Return a dictionnary in which the key is the nb of domains that can be found in a protein,
        and its value correspond to the nb of protein with this nb of domains. 
        Running this function also plot the results with matplotlib. 

        :return final_distri_dict: A dictionnary with the number of domain as key and the number of protein as value. 
        """
        distri_dict = {}
        for protein in self.init_dict:
            nb_domains = len(self.init_dict[protein]["domains"])
            if nb_domains not in distri_dict:
                distri_dict[nb_domains] = 1     # increment a new value for a nb of domains
            else :
                distri_dict[nb_domains] += 1    # increment an existing value of domains
        sorted_distri_dict = sorted(distri_dict)    # sorting the dictionnary by key
        final_distri_dict = {}
        x_val_list = []
        y_val_list = []
        for nb in sorted_distri_dict:
            final_distri_dict[nb] = distri_dict[nb]     # new creation of the sorted dictionanry
            x_val_list.append(nb)
            y_val_list.append(distri_dict[nb])
        plt.bar(x_val_list, y_val_list)
        plt.ylabel("Number of protein with x domain.")
        plt.xlabel("Number of domain.")
        plt.show()
        return final_distri_dict

    def cooccurrence(self, dom_x_str, dom_y_str):
        """ Return the number of cooccurrence of the inputed pair of domains within the interactome.
        More precisely, each time at least one pair is in a protein, the answer get +1.  
        
        :param dom_x_str: The first domain of the interaction.
        :param dom_y_str: The second domaion of the interaction. 
        :return occurence_int: The number of occurence of this interaction. """
        occurence_int = 0
        if dom_x_str == dom_y_str:
            for protein in self.init_dict:
                if self.get_occ(protein, dom_x_str) > 1:
                    occurence_int += 1
        else:
            for protein in self.init_dict:
                if (dom_x_str in self.init_dict[protein]["domains"]) \
                        & (dom_y_str in self.init_dict[protein]["domains"]):
                    occurence_int += 1 
        return occurence_int
    
    def cooccurrence_multi(self, dom_x_str, dom_y_str):
        """ Return the maximum number of cooccurrence of the inputed pair of domains, in a protein.
        This function take into account pairs of same domains.
        
        :param dom_x_str: The first domain of the pair.
        :param dom_y_str: The second domain of the pair.
        :return cooccurrence_int: The max number of occurence of the domains pair."""
        cooccurrence_int = 0
        if dom_x_str == dom_y_str:
            for protein in self.init_dict:
                n = self.get_occ(protein, dom_x_str)
                coocc_int = ((n*(n-1))/2)
                if coocc_int > cooccurrence_int:
                    cooccurrence_int = coocc_int
        else:
            for protein in self.init_dict:
                coocc_int = self.get_occ(protein, dom_x_str)*self.get_occ(protein, dom_y_str)
                if coocc_int > cooccurrence_int:
                    cooccurrence_int = coocc_int                     
        return cooccurrence_int

    def cooccurre_themself(self):
        """ Calculate the number of domain in cooccurrence with themself.

        :return cooccurre_counter: The number of domain in cooccurrence with themself. """
        cooccurre_counter = 0
        for protein in self.init_dict:
            for domain in self.domains_list:
                if self.get_occ(protein, domain) > 1:
                    cooccurre_counter += 1
        return cooccurre_counter

    def generate_cooccurrence_graphe(self):
        """ Create a global graph of all the domains cooccurrence in the whole interactome.
        The cooccurrence is counted when it apears at least once in a protein. 
        
        :return self.domain_graph_list: The domains interactions list"""
        t0 = time.time()
        self.domains_graph_list = []
        self.prot_graph_list = []
        bar = ShadyBar('Global cooccurrence graph is being generated ... ', max=len(self.domains_list))
        for domain1 in self.domains_list:
            for protein in self.domains_dict[domain1]['proteins'][1:]:
                for domain2 in self.init_dict[protein]['domains']:
                    if ((domain1, domain2) not in self.domains_graph_list) \
                            & ((domain2, domain1) not in self.domains_graph_list):
                        self.domains_graph_list.append((domain1, domain2))
                        if protein not in self.prot_graph_list:
                            self.prot_graph_list.append(protein)
            bar.next()
        t1 = time.time()
        print("\n   ==> domains_graph_list variable has been generated in %f seconds."%(t1 - t0))
        print("   ==> There are %d different domains in this graph."%len(self.get_graph_nr_vertex(self.domains_graph_list)))
        print("   ==> There are %d interactions in this graph."%len(self.domains_graph_list))
        return self.domains_graph_list
    
    def generate_cooccurrence_graphe_n(self, n_int):
        """ Create a 'N' graph of all the domains cooccurrence which appears at least 'n_int' time in a protein.

        :param n_int: The minimum number of cooccurrence.       
        :return self.n_domain_graph_list: The domains interactions list"""
        t0 = time.time()
        message = "Cooccurrence graph N with 'n = " + str(n_int) + "' is being generated ... "
        bar = ShadyBar(message, max=len(self.domains_list))
        self.n_domains_graph_list = []
        self.n_prot_graph_list = []
        for domain1 in self.domains_list:
            for protein in self.domains_dict[domain1]['proteins'][1:]:
                for domain2 in self.init_dict[protein]['domains']:
                    if ((domain1, domain2) not in self.n_domains_graph_list) \
                        & ((domain2, domain1) not in self.n_domains_graph_list) \
                            & (self.cooccurrence_multi(domain1, domain2) >= n_int):
                        self.n_domains_graph_list.append((domain1, domain2))
                        if protein not in self.n_prot_graph_list:
                            self.n_prot_graph_list.append(protein)
            bar.next()
        t1 = time.time()
        print("\n   ==> np_domains_graph_list variable has been generated in %f seconds"%(t1 - t0))
        print("   ==> There are %d different domains in this graph."%len(self.get_graph_nr_vertex(self.n_domains_graph_list)))
        print("   ==> There are %d interactions in this graph."%len(self.n_domains_graph_list))
        return self.n_domains_graph_list
    
    def generate_cooccurrence_graphe_np(self, n_int):
        """ Create a 'NP' graph of all the domains cooccurrence which appears at least once in a protein
        and at least 'np_int' time in the whole interactome.

        :param np_int: The minimum number of cooccurrence.       
        :return self.n_domain_graph_list: The domains interactions list"""
        t0 = time.time()
        message = "Cooccurrence graph NP with 'n = " + str(n_int) + "' is being generated ... "
        bar = ShadyBar(message, max=len(self.domains_list))
        self.np_domains_graph_list = []
        self.np_prot_graph_list = []
        for domain1 in self.domains_list:
            for protein in self.domains_dict[domain1]['proteins'][1:]:
                for domain2 in self.init_dict[protein]['domains']:
                    if ((domain1, domain2) not in self.np_domains_graph_list) \
                        & ((domain2, domain1) not in self.np_domains_graph_list) \
                            & (self.cooccurrence(domain1, domain2) >= n_int):
                        self.np_domains_graph_list.append((domain1, domain2))
                        if protein not in self.np_prot_graph_list:
                            self.np_prot_graph_list.append(protein)
            bar.next()
        t1 = time.time()
        print("\n   ==> np_domains_graph_list variable has been generated in %f seconds."%(t1 -t0))
        print("   ==> There are %d different domains in this graph."%len(self.get_graph_nr_vertex(self.np_domains_graph_list)))
        print("   ==> There are %d interactions in this graph."%len(self.np_domains_graph_list))
        return self.np_domains_graph_list

    def density(self, graph_list):
        """ Return the density an interaction graph. You must create a graph_list variable to use this function.
        Please use one of the generate_cooccurrence_graphe-_n-_np functions.
        
        :pram graph_list: The list graph for wich the density will be calculated.
        :return d_float: The density of the graph. """
        domain_nb_int = len(self.get_graph_nr_vertex(graph_list))
        d_float = (2*len(graph_list))/((domain_nb_int)*((domain_nb_int)-1))
        print("\n","Density of the graph : ", d_float)
        return d_float

    def get_neighbors(self, domain_str):
        """ Find all the neighbors of the inputed domain.
        
        :param domain_str: The domain for wich the neighbors are wanted.
        :return neighbors_list: The list of neighbors for the inputed domain. """
        neighbors_list = []
        for protein in self.domains_dict[domain_str]['proteins'][1:]:
            for domain2 in self.init_dict[protein]['domains'].keys():
                if (domain_str == domain2) & (self.get_occ(protein, domain2) > 1):
                    n = self.get_occ(protein, domain2)
                    while n != 1:
                        neighbors_list.append(domain2)
                        n -= 1
                elif (self.get_occ(protein, domain2) > 1) & (domain_str != domain2):
                    n = self.get_occ(protein, domain2)
                    while n != 0:
                        neighbors_list.append(domain2)
                        n = (n-1)
                elif domain_str != domain2:
                    neighbors_list.append(domain2)
        if len(neighbors_list) == 0:
            neighbors_list.append('NA')
        return neighbors_list

    def __xlink_dom_neighbors(self):
        """ Add the key 'neighbors' to the 'domain_dict'. """
        for domain in self.domains_dict:
            self.domains_dict[domain]['neighbors'] = self.get_neighbors(domain)
        return

    def neighbors_max(self, n_int):
        """ Return the top 'n_int' domains having the maximum number of neighbors.
        
        :param n_int: The number of domain to return (length of the output list).
        :return neighbors_list: The list of domains. """
        neighbors_list = []
        n = 0
        top_len_dom_init =''
        i = 100000
        for domain in self.domains_list:
            if len(self.domains_dict[domain]['neighbors']) < i:
                i = len(self.domains_dict[domain]['neighbors'])
                top_len_dom_init = domain
        top_len_dom = top_len_dom_init   
        while n < n_int:
            for domain in self.domains_list:
                if domain not in neighbors_list:
                    if len(self.domains_dict[domain]['neighbors']) \
                        > len(self.domains_dict[top_len_dom]['neighbors']):
                        top_len_dom = domain
            neighbors_list.append(top_len_dom)
            top_len_dom = top_len_dom_init
            n += 1
        return neighbors_list

    def neighbors_min(self, n_int):
        """ Return the top 'n' domains having the minimum number of neighbors.

        :param n_int: The number of domain to return (length of the output list).
        :return neighbors_list: The list of domains. """
        neighbors_list = []
        n = 0
        top_len_dom_init =''
        i = 0
        for domain in self.domains_list:
            if len(self.domains_dict[domain]['neighbors']) > i:
                i = len(self.domains_dict[domain]['neighbors'])
                top_len_dom_init = domain
        top_len_dom = top_len_dom_init
        while n < n_int:
            for domain in self.domains_list:
                if domain not in neighbors_list:
                    if 'NA' in self.domains_dict[domain]['neighbors']:
                        top_len_dom = domain
                    elif len(self.domains_dict[domain]['neighbors']) \
                        < len(self.domains_dict[top_len_dom]['neighbors']):
                        top_len_dom = domain
            neighbors_list.append(top_len_dom)
            top_len_dom = top_len_dom_init
            n += 1
        return neighbors_list

    def most_frequent_dom(self, n_int):
        """ Find the 'n_int' domains the most frequent in whole interactome. 
        
        :param n_int: The number of domains in the output list. 
        :return most_freq_domlist: The list of most frequent domains. """
        n = 0
        most_freq_dom_list = []
        while n < n_int:
            current_domain = ''
            current_occ_int = 0
            for domain in self.domains_dict:
                if (self.domains_dict[domain]['proteins'][0] > current_occ_int) \
                    & (domain not in most_freq_dom_list):
                    current_occ_int = self.domains_dict[domain]['proteins'][0]
                    current_domain = domain
            most_freq_dom_list.append(current_domain)
            n += 1
        return most_freq_dom_list

    def cooccurrence_histogram(self):
        """ Create an histogram of the density in function the minimum number of domain cooccurrence in the graph.
        
        :return (x_list, y_list): A tuple of lists with 'x_list' the minimum number of domains cooccurrence 
        and 'y_list' the density of the graph. """
        x_list = [1, 2, 4, 8, 16, 32]
        y_list = []
        for n_int in x_list:
            graph = self.generate_cooccurrence_graphe_np(n_int)
            y_list.append(self.density(graph))
        plt.plot(x_list, y_list)
        plt.show()
        return (x_list, y_list)

    def mean_dom_prot(self, n_int):
        """ Calculate the mean number of domain in each protein with at least 'n_int' domains.

        :param n_int: The minimum number of domain.
        :return mean_dom_prot_float: The mean number of domains per protein.  
        """
        nb_domains_list =[]
        total = 0
        for protein in self.init_dict:
            nb = 0
            for domain in self.init_dict[protein]['domains']:
                if domain != 'NA':
                    nb += self.init_dict[protein]['domains'][domain][1]
            if nb >= n_int:
                nb_domains_list.append(nb)
                total += nb
        print(nb_domains_list)
        mean_dom_prot_float = total/len(nb_domains_list)
        return mean_dom_prot_float
    
    def max_dom_prot(self):
        """ Find the maximum number of domain in a protein and the related protein to this number.

        :return max_dom_prot_list: A list with at postion 0 the max number of domain and postion 1 the name of the related protein.    
        """
        nb_domains_list =[]
        max_dom_prot_list = [0,0]
        i_index = 0
        for protein in self.init_dict:
            nb = 0
            for domain in self.init_dict[protein]['domains']:
                if domain != 'NA':
                    nb += self.init_dict[protein]['domains'][domain][1]
            nb_domains_list.append(nb)
        for i in nb_domains_list:
            if i > max_dom_prot_list[0]:
                max_dom_prot_list[0] = i
                max_dom_prot_list[1] = i_index
            i_index += 1
        max_dom_prot_list[1] = list(self.init_dict.keys())[max_dom_prot_list[1]]
        return max_dom_prot_list

    def get_web_page(self, molecule_type_str, molecule_name_str):
        """ Get the URL of the wanted domain (pfam database) or protein (uniprot database).
        
        :param molecule_type_str: 'domain' or 'protein', the type of molecule.
        :param molecule_name_str: The name of the molecule.
        :return url: The url of the domain (pfam) or the protein (uniprot)."""
        url = "NO URL because of a KeyError"
        try:
            if molecule_type_str == 'protein':
                url = 'https://www.uniprot.org/uniprot/'+self.init_dict[molecule_name_str]['uniprot_id']
            elif molecule_type_str == 'domain':
                domain_graph = DomainGraph()
                url = 'http://pfam.xfam.org/family/'+domain_graph.domains_dict[molecule_name_str]['pfam_id']
            else:
                print("Please write as first parameter 'protein' or 'domain'.")
        except KeyError:
                    print("KEY ERROR:  Please enter a valid molecule name.")
        return url
