
#!/usr/bin/python3
from interactome import Interactome
import unittest


#x = Interactome("Human_HighQuality.txt")
#x.count_cc()
#x.extract_cc('P85A_HUMAN')
##print(len(x.compute_cc()))
##print(len(x.proteins_list))
##print(len(x.init_dict))
##print(len(x.cc_dict))
#x.density()
#x.clustering('P85A_HUMAN')
#print(x.count_degree(60))

#x = Interactome('data_interactome/1_cc_full_interactions.txt')
#print("GET DEGREE", x.get_degree('Cx'))
#print("MAX DEGREE", x.get_max_degree())
#print("AVE DEGREE", x.get_ave_degree())
#print("COUNT DEGREE", x.count_degree(1))
#print("HISTO",x.histogram_degree(1,2))
#print("EXTRACT",x.extract_cc('Cx'))
#print("COUNT", x.count_cc())
#x.density()
#print("COMPUTE",x.compute_cc())
#x.clustering('Cx')


class Test_Interactome(unittest.TestCase):
	def test_1_cc_9_interactions(self):
		interactome = Interactome('data_interactome/1_cc_9_interactions.txt')
		self.assertEqual(interactome.get_degree('Cx'), 4, "Should be 4")
		self.assertEqual(interactome.get_max_degree(), ('Ax', 5), "Should be ('Ax', 5)")
		self.assertEqual(interactome.get_ave_degree(), 2.25, "Should be 2.25")
		self.assertEqual(interactome.count_degree(1), (4, ['Ex', 'Dx', 'Fx', 'Gx']), "Should be (4, ['Ex', 'Dx', 'Fx', 'Gx'])")
		self.assertEqual(interactome.histogram_degree(1,2), 5, "Should be 5")
		self.assertEqual(interactome.extract_cc('Cx'), ['Ax', 'Bx', 'Cx', 'Ex', 'Gx', 'Hx', 'Dx', 'Fx'], "Should be ['Ax', 'Bx', 'Cx', 'Ex', 'Gx', 'Hx', 'Dx', 'Fx']")
		self.assertEqual(interactome.count_cc(), {1: (8, ['Ax', 'Bx', 'Cx', 'Ex', 'Gx', 'Hx', 'Dx', 'Fx'])}, "Should be {1: (8, ['Ax', 'Bx', 'Cx', 'Ex', 'Gx', 'Hx', 'Dx', 'Fx'])}")
		self.assertEqual(interactome.density(), 0.32142857142857145, "Should be 0.32142857142857145")
		self.assertEqual(interactome.compute_cc(), [1, 1, 1, 1, 1, 1, 1, 1], "Should be [1, 1, 1, 1, 1, 1, 1, 1]")
		self.assertEqual(interactome.clustering('Cx'), 0.3333333333333333, "Should be 0.3333333333333333")
	def test_3_cc_9_interactions(self):
		interactome = Interactome('data_interactome/3_cc_9_interactions.txt')
		self.assertEqual(interactome.get_degree('Cx'), 2, "Should be 2")
		self.assertEqual(interactome.get_max_degree(), ('Gx', 3), "Should be ('Gx', 3)")
		self.assertEqual(interactome.get_ave_degree(), 1.8333333333333333, "Should be 1.8333333333333333")
		self.assertEqual(interactome.count_degree(1), (3, ['Ax', 'Ex', 'Lx']), "Should be (3, ['Ax', 'Ex', 'Lx'])")
		self.assertEqual(interactome.histogram_degree(1,2), 11, "Should be 11")
		self.assertEqual(interactome.extract_cc('Cx'), ['Bx', 'Dx', 'Hx', 'Cx'], "Should be ['Bx', 'Dx', 'Hx', 'Cx']")
		self.assertEqual(interactome.count_cc(), {1: (5, ['Ax', 'Fx', 'Gx', 'Ix', 'Jx']), 2: (4, ['Bx', 'Dx', 'Hx', 'Cx']), 3: (3, ['Ex', 'Kx', 'Lx'])}, "Should be {1: (5, ['Ax', 'Fx', 'Gx', 'Ix', 'Jx']), 2: (4, ['Bx', 'Dx', 'Hx', 'Cx']), 3: (3, ['Ex', 'Kx', 'Lx'])}")
		self.assertEqual(interactome.density(), 0.16666666666666666, "Should be 0.16666666666666666")
		self.assertEqual(interactome.compute_cc(), [1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3], "Should be [1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3]")
		self.assertEqual(interactome.clustering('Cx'), 0.0, "Should be 0.0")
	def test_1_cc_full_interactions(self):
		interactome = Interactome('data_interactome/1_cc_full_interactions.txt')
		self.assertEqual(interactome.get_degree('Cx'), 4, "Should be 4")
		self.assertEqual(interactome.get_max_degree(), ('Ax', 4, 'Bx', 4, 'Cx', 4, 'Dx', 4, 'Ex', 4), "Should be ('Ax', 4, 'Bx', 4, 'Cx', 4, 'Dx', 4, 'Ex', 4)")
		self.assertEqual(interactome.get_ave_degree(), 4.0, "Should be 4.0")
		self.assertEqual(interactome.count_degree(1), (0, []), "Should be (0, [])")
		self.assertEqual(interactome.histogram_degree(1,2), 0, "Should be 0")
		self.assertEqual(interactome.extract_cc('Cx'), ['Ax', 'Bx', 'Cx', 'Dx', 'Ex'], "Should be ['Ax', 'Bx', 'Cx', 'Dx', 'Ex']")
		self.assertEqual(interactome.count_cc(), {1: (5, ['Ax', 'Bx', 'Cx', 'Dx', 'Ex'])}, "Should be {1: (5, ['Ax', 'Bx', 'Cx', 'Dx', 'Ex'])}")
		self.assertEqual(interactome.density(), 1.0, "Should be 1.0")
		self.assertEqual(interactome.compute_cc(), [1, 1, 1, 1, 1], "Should be [1, 1, 1, 1, 1]")
		self.assertEqual(interactome.clustering('Cx'), 1.0, "Should be 1.0")
	def test_Human_HighQuality(self):
		prot_name = "INSR_HUMAN"
		interactome = Interactome('data_interactome/Human_HighQuality.txt')
		interactome.xlink_uniprot('data_interactome/Human_Proteome.txt')
		self.assertEqual(interactome.get_protein_domain(interactome.init_dict[prot_name]['uniprot_id']), {'Furin-like': ['PF00757', 1], 'Insulin_TMD': ['PF17870', 1], 'PK_Tyr_Ser-Thr': ['PF07714', 1], 'Recep_L_domain': ['PF01030', 2]}, "Should be {'Furin-like': ['PF00757', 1], 'Insulin_TMD': ['PF17870', 1], 'PK_Tyr_Ser-Thr': ['PF07714', 1], 'Recep_L_domain': ['PF01030', 2]}")

			
if __name__ == '__main__':
    unittest.main()	